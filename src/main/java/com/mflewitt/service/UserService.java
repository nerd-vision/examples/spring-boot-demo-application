package com.mflewitt.service;

import com.mflewitt.model.User;
import java.util.List;
import org.springframework.security.core.parameters.P;

public interface UserService {
    void save(User user);

    User findById( Long id );

    User findByUsername( String username);

    User searchName( String name );

    void makeAdmin(Long id);

    List<User> findAll();
}
