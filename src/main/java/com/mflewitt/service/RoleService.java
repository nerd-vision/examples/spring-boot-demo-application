package com.mflewitt.service;

import com.mflewitt.model.Role;

public interface RoleService
{
    void save( Role role);

    Role findByName(String name);
}
