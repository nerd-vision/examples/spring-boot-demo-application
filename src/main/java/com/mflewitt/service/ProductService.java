package com.mflewitt.service;

import com.mflewitt.model.Product;
import java.util.List;

public interface ProductService
{
    Product findByID(Long id);

    void save( Product product);

    void delete(Product product);

    Product searchName(String name);

    List<Product> findAll();

    void restock(Product product, int quantity);
}
