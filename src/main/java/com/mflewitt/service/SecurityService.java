package com.mflewitt.service;

public interface SecurityService
{
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
