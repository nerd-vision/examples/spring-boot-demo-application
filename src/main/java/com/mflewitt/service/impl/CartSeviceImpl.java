package com.mflewitt.service.impl;

import com.mflewitt.model.Cart;
import com.mflewitt.model.Product;
import com.mflewitt.model.User;
import com.mflewitt.repository.CartRepository;
import com.mflewitt.repository.ProductRepository;
import com.mflewitt.service.CartService;
import com.mflewitt.service.ProductService;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartSeviceImpl implements CartService
{
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductService productService;



    @Override
    public void addProduct( User user, Product product )
    {
        Cart cart = cartRepository.findDistinctByUserIsAndProductIs( user, product );
        if( cart != null )
        {
            cart.setQuantity( cart.getQuantity() + 1 );
        }
        else
        {
            cart = new Cart();
            cart.setProduct( product );
            cart.setUser( user );
            cart.setQuantity( 1 );
        }
        cartRepository.save( cart );
    }


    @Override
    public void removeProduct( final User user, final Product product )
    {
        Cart cart = cartRepository.findDistinctByUserIsAndProductIs( user, product );
        if( cart != null )
        {
            cartRepository.delete( cart );
        }
    }


    @Override
    public void increaseQuantity( final User user, final Product product )
    {
        Cart cart = cartRepository.findDistinctByUserIsAndProductIs( user, product );
        if( cart != null )
        {
            cart.setQuantity( cart.getQuantity() + 1 );
        }
        else
        {
            addProduct( user, product );
        }

        cartRepository.save( cart );
    }


    @Override
    public void decreaseQuality( final User user, final Product product )
    {
        Cart cart = cartRepository.findDistinctByUserIsAndProductIs( user, product );
        if( cart != null && cart.getQuantity() > 1)
        {
            cart.setQuantity( cart.getQuantity() - 1 );
        }
        else
        {
            removeProduct( user, product );
        }
    }


    @Override
    public void buyNow( final User user )
    {
        List<Cart> carts = cartRepository.findCartsByUserIs( user );

        for( Cart cart : carts )
        {
            Product product = cart.getProduct();
            int inititialQuantity = product.getQuantity();
            product.setQuantity( product.getQuantity() - cart.getQuantity() );

            if( (product.getQuantity() / inititialQuantity) < 0.1  )
            {
                int restockAmmount = inititialQuantity + product.getQuantity();
                if( product.getQuantity() == 0 )
                {
                    restockAmmount = inititialQuantity * 2;
                }
                productService.restock( product, restockAmmount );
            }

            productRepository.save( product );
            cartRepository.delete( cart );
        }
    }


    @Override
    public BigDecimal getTotal( final User user )
    {
        List<Cart> carts = cartRepository.findCartsByUserIs( user );
        BigDecimal total = BigDecimal.valueOf( 0 );

        for( Cart cart : carts )
        {
            double discount = 1.00;
            if ( cart.getProduct().getName() == "Storage" )
            {
                discount = 0.9;
            }

            BigDecimal productTotal = cart.getProduct().getPrice().multiply( BigDecimal.valueOf( cart.getQuantity() * discount));
            total = total.add( productTotal );
        }

        return total;
    }

    @Override
    public List<Cart> getProductsinCart(User user)
    {
        return cartRepository.findCartsByUserIs( user );
    }
}
