package com.mflewitt.service.impl;

import com.mflewitt.model.Product;
import com.mflewitt.repository.ProductRepository;
import com.mflewitt.service.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService
{
    @Autowired
    private ProductRepository productRepository;


    @Override
    public Product findByID(Long id)
    {
        return productRepository.getOne( id );
    }

    @Override
    public void save( Product product )
    {
        productRepository.save( product );
    }


    @Override
    public void delete( final Product product )
    {
        productRepository.delete( product );
    }


    @Override
    public Product searchName( final String name )
    {
        return productRepository.findByNameLike( name );
    }


    @Override
    public List<Product> findAll()
    {
        return productRepository.findAll();
    }


    @Override
    public void restock( final Product product, int quantity )
    {
        //ToDo
        //order new stock

        product.setQuantity( product.getQuantity() + quantity );
        productRepository.save( product );
    }
}
