package com.mflewitt.model;

import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="user")
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String password;

    @Transient
    private String passwordConf;

    @ManyToOne
    private Role role;

    public Long getId()
    {
        return id;
    }


    public void setId( final Long id )
    {
        this.id = id;
    }


    public String getUsername()
    {
        return username;
    }


    public void setUsername( final String username )
    {
        this.username = username;
    }


    public String getPassword()
    {
        return password;
    }


    public void setPassword( final String password )
    {
        this.password = password;
    }


    public String getPasswordConf()
    {
        return passwordConf;
    }


    public void setPasswordConf( final String passwordConf )
    {
        this.passwordConf = passwordConf;
    }


    public Role getRole()
    {
        return role;
    }


    public void setRole( final Role role )
    {
        this.role = role;
    }
}
