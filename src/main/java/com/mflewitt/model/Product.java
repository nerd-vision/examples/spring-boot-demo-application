package com.mflewitt.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;

@Entity
@Table(name="product")
public class Product
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String description;

    private int quantity;
    @DecimalMin(value = "0.00", message = "price is a positive number")
    private BigDecimal price;

    public long getId()
    {
        return id;
    }


    public void setId( final long id )
    {
        this.id = id;
    }


    public String getName()
    {
        return name;
    }


    public void setName( final String name )
    {
        this.name = name;
    }


    public String getDescription()
    {
        return description;
    }


    public void setDescription( final String description )
    {
        this.description = description;
    }


    public int getQuantity()
    {
        return quantity;
    }


    public void setQuantity( final int quantity )
    {
        this.quantity = quantity;
    }


    public BigDecimal getPrice()
    {
        return price;
    }


    public void setPrice( final BigDecimal price )
    {
        this.price = price;
    }
}
