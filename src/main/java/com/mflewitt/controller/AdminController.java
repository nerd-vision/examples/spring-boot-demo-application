package com.mflewitt.controller;

import com.mflewitt.model.Product;
import com.mflewitt.service.ProductService;
import com.mflewitt.service.UserService;
import com.mflewitt.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AdminController
{
    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductValidator productValidator;

    @GetMapping("/admin")
    public String admin()
    {
        return "admin";
    }

    @GetMapping("/admin/product")
    public String products( Model model)
    {
        model.addAttribute("productForm", new Product());
        model.addAttribute( "products", productService.findAll() );
        return "products";
    }

    @PostMapping("/admin/product")
    public String registration( @ModelAttribute("productForm") Product productForm, BindingResult productBindingResult, Model model) {
        productValidator.validate(productForm, productBindingResult);

        if (productBindingResult.hasErrors()) {
            model.addAttribute( "products", productService.findAll() );
            return "registration";
        }

        productService.save(productForm);

        return "redirect:/admin/product";
    }

    @DeleteMapping("admin/product/{id}")
    public String delete( @PathVariable Long id)
    {
        productService.delete( productService.findByID( id ) );

        return "redirect:/admin/product";
    }

    @GetMapping("admin/product/{id}")
    public String edit(@PathVariable Long id, Model model)
    {
        model.addAttribute( "productForm", productService.findByID( id ) );
        model.addAttribute( "products", productService.findAll() );
        return "products";
    }

    @GetMapping("/admin/user")
    public String users( Model model )
    {
        model.addAttribute( "users", userService.findAll() );
        return "users";
    }

    @PostMapping("admin/user/{id}")
    public String makeAdmin(@PathVariable Long id)
    {
        userService.makeAdmin( id );

        return "redirect:/admin/user";
    }
}
