package com.mflewitt.validator;

import com.mflewitt.model.Product;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProductValidator implements Validator
{
    @Override
    public boolean supports( final Class<?> aClass )
    {
        return Product.class.equals( aClass );
    }


    @Override
    public void validate( final Object o, final Errors errors )
    {
        Product product = (Product) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (product.getDescription().length() < 5) {
            errors.rejectValue("description", "Size.productForm.description");
        }
        if (product.getName().length() < 5) {
            errors.rejectValue("name", "Size.productForm.name");
        }
        if( product.getPrice().intValue() < 0 )
        {
            errors.rejectValue("price", "Value.productForm.price");
        }
        if( product.getQuantity() < 0 )
        {
            errors.rejectValue("price", "Value.productForm.quantity");
        }
    }
}
