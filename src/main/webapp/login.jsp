<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Log in with your account</title>

    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="${contextPath}/resources/css/common.css" rel="stylesheet" type = "text/css">

</head>

<body>

<div class="container">
    <form method="POST" action="${contextPath}/login" class="form-signin">
        <div class="login-container">
            <h1 style="text-align:center; margin-bottom:70px">Shop Demo</h1>
            <div class="form-group ${error != null ? 'has-error' : ''}">
                <span>${message}</span>
                <input style="margin-bottom:10px;" name="username" type="text" class="form-control" placeholder="Username"
                       autofocus="true"/>
                <input style="margin-bottom:10px;" name="password" type="password" class="form-control" placeholder="Password"/>
                <span style="margin-bottom:10px;">${error}</span>
                <input style="margin-bottom:10px;" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
                <h4 class="text-center"><a href="${contextPath}/registration">Create an account</a></h4>
            </div>
        </div>
    </form>
</div>

<div style="text-align: center">
    <b>Existing admin user</b>
    <p>Username : admin</p>
    <p>Password : admin1234</p>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>


<!-- Latest compiled and minified bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>