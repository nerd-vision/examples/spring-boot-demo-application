<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Products</title>
</head>
<body>
<jsp:include page="header.jsp" />
<div class="container">
    <H1>Current Products</H1>
    <table>
        <tr>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Product Price</th>
            <th>Product Quantity</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="product" items="${products}">
            <tr>
                <td>"${product.name}"</td>
                <td>"${product.description}"</td>
                <td>"${product.price}"</td>
                <td>"${product.quantity}"</td>
                <td>
                    <form:form method = "DELETE" action = "/admin/product/${product.id}">
                        <table>
                            <tr>
                                <td>
                                    <input type = "submit" value = "Delete Product"/>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                    <form:form method = "GET" action = "/admin/product/${product.id}">
                        <table>
                            <tr>
                                <td>
                                    <input type = "submit" value = "Edit Product"/>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </td>

            </tr>
        </c:forEach>
    </table>
</div>

<div class="container">
    <H1>Add Product</H1>#

    <form:form method="POST" modelAttribute="productForm" action="/admin/product">
        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="name" class="form-control" placeholder="Name"
                            autofocus="true"></form:input>
                <form:errors path="name"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="description">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="description" class="form-control" placeholder="Description"
                            autofocus="true"></form:input>
                <form:errors path="description"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="price">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="number" step="0.01" path="price" class="form-control"
                            placeholder="Price"></form:input>
                <form:errors path="price"></form:errors>
            </div>
        </spring:bind>
        <spring:bind path="quantity">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="number" path="quantity" class="form-control" placeholder="Quantity"></form:input>
                <form:errors path="quantity"></form:errors>
            </div>
        </spring:bind>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Add / Edit Product</button>
    </form:form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>