<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Shop</title>
</head>
<body>
<jsp:include page="header.jsp" />
<div>

    <div class="container product-container">
        <div class="row">

            <c:forEach var="product" items="${products}">

                <div class="col-md-6">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2>${product.name}</h2>
                        </div>
                        <div class="panel-body">
                            <h3>${product.description}</h3>
                            <h3>${product.price} Euro - ${product.quantity} Remaining</h3>
                        </div>
                        <div class="panel-footer">
                           <form:form method = "GET" action = "/cart/${product.id}">
                                <table>
                                    <tr>
                                        <td>
                                            <input type = "submit" value = "Add to Cart"/>
                                        </td>
                                    </tr>
                                </table>
                            </form:form>
                        </div>
                    </div>

                </div>

            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>