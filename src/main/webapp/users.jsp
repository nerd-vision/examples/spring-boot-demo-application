<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Users</title>
</head>
<body>
<jsp:include page="header.jsp" />
<div class="">

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                    <table class="table">
                        <tr>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                        <c:forEach var="user" items="${users}">
                            <tr>
                                <td>"${user.username}"</td>
                                <td>"${user.role.name}"</td>
                                <td>
                                    <form:form method = "POST" action = "/admin/user/${user.id}">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input class="make-admin-button" type = "submit" value = "Make Admin"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form:form>
                                </td>

                            </tr>
                        </c:forEach>
                    </table>

            </div>
        </div>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>