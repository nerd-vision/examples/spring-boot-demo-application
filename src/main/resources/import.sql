INSERT INTO `role` (`id`, `name`) VALUES ('1', 'customer');
INSERT INTO `role` (`id`, `name`) VALUES ('2', 'admin');
INSERT INTO `user` (`id`, `password`, `username`, `role_id`) VALUES ('1', '$2a$10$5/ahXcm6Lo3mXvbJdYu.TuIjkzZDqbr4fZ97j34QQGCqWQsADOI6.', 'user1', '1');
INSERT INTO `user` (`id`, `password`, `username`, `role_id`) VALUES ('2', '$2a$10$5/ahXcm6Lo3mXvbJdYu.TuIjkzZDqbr4fZ97j34QQGCqWQsADOI6.', 'user2', '1');
INSERT INTO `user` (`id`, `password`, `username`, `role_id`) VALUES ('3', '$2a$10$5/ahXcm6Lo3mXvbJdYu.TuIjkzZDqbr4fZ97j34QQGCqWQsADOI6.', 'admin', '2');
INSERT INTO `product` (`id`, `description`, `name`, `price`, `quantity`) VALUES ('1', '1TB Solid State Drive', 'Storage', '151.65', '126');
INSERT INTO `product` (`id`, `description`, `name`, `price`, `quantity`) VALUES ('2', '64GB USB Stick', 'USB Stick (64GB)', '5.63', '521');
INSERT INTO `product` (`id`, `description`, `name`, `price`, `quantity`) VALUES ('3', '32GB USB Stick', 'USB Stick (32GB)', '3.52', '10');